/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sequencia;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class TelaController implements Initializable {
    
    private ArrayList<Interesse> interesses = new ArrayList();
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField nomeT;
    @FXML
    private PasswordField senhaT;
    @FXML
    private TextArea interessesT;
    @FXML
    private PasswordField reSenhaT;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        if(!senhaT.getText().equals(reSenhaT.getText()))
        {
            System.out.println("Senhas não coincidem");
        }
        else{
            Usuario u = new Usuario(nomeT.getText(), senhaT.getText().hashCode());
            u.insert();
            for(String s : interessesT.getText().split(","))
            {
                interesses.add(new Interesse(u, s));
            }       
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}
