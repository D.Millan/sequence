/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sequencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Usuario {
    private int id, senha;
    private String nome;
    private ArrayList interesses = new ArrayList();
    
    public Usuario(String nome, int senha)
    {
        this.senha = senha;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenha() {
        return senha;
    }

    public void setSenha(int senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void insert()
    {
        String SQL = "INSERT INTO USUARIO (id, nome, senha) values (idSeq.nextval, ?, ?)";
            String[] col = {"id"};
            try{
                PreparedStatement ps = Conexao.get().prepareStatement(SQL, col);
                ps.setString(1, nome);
                ps.setInt(2, senha);
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                if(rs.next())
                {
                    id = rs.getInt(1);
                }
                
            }
            catch(Exception e)
            {
                System.out.println("Erro de banco");   
            }
    }
    
    @Override
    public boolean equals(Object u)
    {
        if(id == ((Usuario) u).getId()){
            return true;
        } 
        return false;
    }
    
}
