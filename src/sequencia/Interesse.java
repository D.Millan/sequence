/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sequencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Aluno
 */
public class Interesse {
    private String interesse;
    private int id;
    private Usuario user;
    
    public Interesse(Usuario user, String interesse)
    {
     this.user = user;
     this.interesse = interesse;
     insert();
    }
    
    public void insert()
    {
        String SQL = "INSERT INTO INTERESSE (id, iduser, interesse) values (intSeq.nextval, ?, ?)";
            String[] col = {"id"};
            try{
                PreparedStatement ps = Conexao.get().prepareStatement(SQL, col);
                ps.setInt(1, this.user.getId());
                ps.setString(2, interesse);
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                if(rs.next())
                {
                    id = rs.getInt(1);
                }
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());   
            }
    }
}
